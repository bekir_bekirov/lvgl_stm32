file (GLOB SOURCES_STM32F429 
*.c
portable/*.c
)
message (STATUS "SOURCES_STM32F429: - ${SOURCES_STM32F429}")

add_library(freertos_stm32_lib STATIC ${SOURCES_STM32F429})
add_library(${TARGET}::freertos_stm32_lib ALIAS freertos_stm32_lib)

target_include_directories(freertos_stm32_lib PUBLIC
.
portable
include
)

#target_compile_definitions(freertos_stm32_lib PUBLIC USE_HAL_DRIVER STM32F4)

message(STATUS "INCLUDE_DIRS_LIB_freertos_stm32_lib: - \
.
portable
include
"
)

target_link_libraries(freertos_stm32_lib PUBLIC
${TARGET}::hal_stm32_lib
)
