/**
 * @file lv_draw.h
 *
 */

#ifndef LV_DRAW_H
#define LV_DRAW_H

#ifdef __cplusplus
extern "C" {
#endif

/*********************
 *      INCLUDES
 *********************/
#include "../lv_conf_internal.h"

#include "../lv_core/lv_style.h"
#include "../lv_misc/lv_txt.h"
#include "../../../../lvgl_lib/lvgl/src/lv_draw/lv_img_decoder.h"

#include "../../../../lvgl_lib/lvgl/src/lv_draw/lv_draw_rect.h"
#include "../../../../lvgl_lib/lvgl/src/lv_draw/lv_draw_label.h"
#include "../../../../lvgl_lib/lvgl/src/lv_draw/lv_draw_img.h"
#include "../../../../lvgl_lib/lvgl/src/lv_draw/lv_draw_line.h"
#include "../../../../lvgl_lib/lvgl/src/lv_draw/lv_draw_triangle.h"
#include "../../../../lvgl_lib/lvgl/src/lv_draw/lv_draw_arc.h"
#include "../../../../lvgl_lib/lvgl/src/lv_draw/lv_draw_blend.h"
#include "../../../../lvgl_lib/lvgl/src/lv_draw/lv_draw_mask.h"

/*********************
 *      DEFINES
 *********************/

/**********************
 *      TYPEDEFS
 **********************/

/**********************
 * GLOBAL PROTOTYPES
 **********************/

/**********************
 *  GLOBAL VARIABLES
 **********************/

/**********************
 *      MACROS
 **********************/

/**********************
 *   POST INCLUDES
 *********************/

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /*LV_DRAW_H*/
